const { MatrixClient, SimpleFsStorageProvider, AutojoinRoomsMixin, LogLevel, LogService, RichConsoleLogger, PantalaimonClient } = require ('matrix-bot-sdk');
const { Command, CommandManager } = require ('./manager');
const config = require ('../config/config.json');

LogService.setLogger (new RichConsoleLogger ());
LogService.setLevel (LogLevel.INFO);
LogService.info ('bot', 'configuring the bot...');

(async function () {
    const storage = new SimpleFsStorageProvider (config.storage);
    let client = null;

    // setup pantalaimon
    if (config.pantalaimon.use) {
        LogService.info ('bot', 'setting up pantalaimon connection...');

        const pantalaimon = new PantalaimonClient (config.homeserver, storage);
        client = await pantalaimon.createClientWithCredentials (config.pantalaimon.username, config.pantalaimon.password);
    } else {
        LogService.info ('bot', 'pantalaimon is disabled');
        client = new MatrixClient (config.homeserver, config.token, storage);
    }

    // auto accept any room invites
    AutojoinRoomsMixin.setupOnClient (client);

    // setup command handler
    const commands = new CommandManager (client);
    await commands.start ();

    // run client
    try {
        await client.start ();
        LogService.info ('bot', 'bot started successfully');
    } catch (e) {
        LogService.error ('bot', 'the bot failed to start');
    }
}) ();