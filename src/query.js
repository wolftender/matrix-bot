const { LogService, RichReply } = require ('matrix-bot-sdk');
const { Command } = require ('./command');

const gelbooru = require ('./gelbooru');
const https = require ('https');
const mime = require ('mime-types');
const fs = require ('fs');

const config = require ('../config/config.json');

class QueryCommand extends Command {
    constructor (client, legacy = false) {
        super (client);

        this.legacy = legacy;
        if (this.legacy) {
            LogService.warn ('query', 'the query command is running in legacy mode');
        }

        LogService.info ('query', 'query command was initialized');
    }

    async download (url, path) {
        return new Promise ((resolve, reject) => {
            let filestream = fs.createWriteStream (path);
            https.get (url, (res) => { 
                res.pipe (filestream); 

                res.on ('end', () => {
                    resolve (path);
                });
            }).on ('error', reject);
        });
    }

    async run (roomId, event, args, raw, manager) {
        const tags = args.join (' ');
        const tagLength = tags.replaceAll(/[^A-Za-z\(\)\:0-9]/gm, '').length;

        if (tags.length > 512) {
            const newEvent = RichReply.createFor (roomId, event, null, `your query contains too many tags!`);
            this.client.sendMessage (roomId, newEvent);

            LogService.warn ('query', `received a query from ${event.sender} with too many tags`);
            return;
        }

        LogService.info ('query', `received search request from ${event.sender}`);

        if (!tagLength) {
            const newEvent = RichReply.createFor (roomId, event, null, `please provide valid tags for search`);
            this.client.sendMessage (roomId, newEvent);
            return;
        }

        const data = await gelbooru.getRandomImage (tags, this.legacy);

        if (!data) {
            const newEvent = RichReply.createFor (roomId, event, null, `no results found for <b>${tags}</b>`);
            this.client.sendMessage (roomId, newEvent);
            return;
        }

        const type = data.file_url.split ('.').pop ();

        if (['png', 'jpg', 'gif', 'jpeg'].indexOf (type) === -1) {
            /*this.getClient ().sendMessage (roomId, {
                'msgtype': 'm.text',
                'body': `the file found cannot be displayed: ${data.file_url}`
            });*/
            const newEvent = RichReply.createFor (roomId, event, null, `the file found cannot be displayed: ${data.file_url}`);
            this.client.sendMessage (roomId, newEvent);
            return;
        }

        // select resolution based on bot configuration
        let file_url = data.file_url;
        if (config.resolution === 'sample' && data.sample_url) {
            file_url = data.sample_url;
        }

        if (config.resolution === 'preview' && data.preview_url) {
            file_url = data.preview_url;
        }

        LogService.info ('query', `sending file ${file_url}`);
        
        // upload and send the file
        const mxc = await this.client.uploadContentFromUrl (file_url);

        await this.client.sendMessage (roomId, {
            msgtype: 'm.image',
            body: `${data.id}`,
            url: mxc,
            info: { 
                mimetype: mime.lookup (type)
            }
        });

        return;
    }
}

module.exports = { QueryCommand };