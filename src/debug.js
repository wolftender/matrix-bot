const { LogService, RichReply } = require ('matrix-bot-sdk');
const { Command } = require ('./command');

class DebugCommand extends Command {
    constructor (client) {
        super (client);
        LogService.info ('debug', 'debug command was initialized');
    }

    async run (roomId, event, args, raw, manager) {
        if (args.length == 1 && args[0] == 'throw') {
            throw new Error ('test exception');
        }

        const newEvent = RichReply.createFor (roomId, event, null, `<p>the <b>debug command</b> was run with following parameters:
        <ul>
            <li>roomId = ${roomId}</li>
            <li>args = ${args}</li>
            <li>raw = ${raw}</li>
        </ul>
        </p>`);

        this.client.sendMessage (roomId, newEvent);
    }
}

module.exports = { DebugCommand };