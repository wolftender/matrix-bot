const { UserID, LogService, RichReply } = require ('matrix-bot-sdk');
const { QueryCommand } = require ('./query');
const { DebugCommand } = require ('./debug');
const { Command } = require ('./command');

const config = require ('../config/config.json');

class CommandManager {
    async start () {
        if (this.started) return;

        this.userId = await this.client.getUserId ();
        this.localpart = new UserID (this.userId).localpart;

        this.started = true;
        this.client.on ('room.message', this.handleMessage.bind (this));

        // register commands
        this.registerCommand ('gel', new QueryCommand (this.client));
        
        // only in debug mode
        if (config.debug) {
            this.registerCommand ('debug', new DebugCommand (this.client));
        }
    }

    setCooldown (sender, value) {
        this.__cooldowns [sender] = value;
    }

    getCooldown (sender) {
        return !!this.__cooldowns [sender];
    }

    constructor (client) {
        this.__handlers = { };
        this.__cooldowns = { };

        this.client = client;
        this.started = false;
    }

    registerCommand (command, handler) {
        if (this.__handlers [command]) {
            throw new Error (`${command} was already registered`);
        }

        if (!handler instanceof Command) {
            throw new Error (`${command} was not supplied with valid handler`);
        }

        this.__handlers [command] = handler;
    }

    async handleMessage (roomId, event) {
        if (!this.started) {
            throw new Error ('the command handler was not started');
        }

        if (event.isRedacted) return;
        if (event.sender === this.userId) return;
        if (!event.content) return;
        if (event.content.msgtype !== 'm.text') return;

        const body = event.content.body;
        if (!body.startsWith (config.prefix)) return;

        const sender = event.sender;
        if (this.getCooldown (sender)) {
            LogService.info ('command', `${sender} is on cooldown`);
            return;
        }

        this.setCooldown (sender, true);

        const raw = body.trim ();
        const args = raw.split (/\s+/);

        if (args.length < 1 || args[0].length <= config.prefix.length) return;

        const command = args[0].substring (config.prefix.length);
        const rawArgs = raw.substring (args[0].length).trim ();

        let errorOccured = false;
        if (args.length >= 1) {
            if (this.__handlers [command]) {
                const perfStart = performance.now();
                LogService.info ('command', `received command !${command} from ${event.sender} in ${roomId}`);

                try {
                    await this.__handlers [command].run (roomId, event, args.slice (1), rawArgs, this);
                } catch (error) {
                    LogService.error ('command', `there was an error while executing command !${command}, check below for details`);
                    LogService.error ('command', error);
                    errorOccured = true;
                }

                const perfEnd = performance.now ();
                const execTime = (perfEnd - perfStart).toFixed (3);
                LogService.info ('command', `finished executing command !${command} in ${execTime}ms`);
            } else {
                LogService.info ('command', `received command !${command} from ${event.sender} in ${roomId}, but it does not exist`);
            }
        }

        this.setCooldown (sender, false);

        if (errorOccured) {
            const newEvent = RichReply.createFor (roomId, event, `error`, `<p>an error has occured, please try again later</p>`);
            this.client.sendMessage (roomId, newEvent);
        }
    }
}

module.exports = { CommandManager, Command };