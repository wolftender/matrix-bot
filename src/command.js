class Command {
    constructor (client) {
        this.client = client;
    }

    getClient () {
        return this.client;
    }

    async run (roomId, event, args, raw, manager) {
        throw new Error ('method run() was not implemented');
    }
}

module.exports = { Command };