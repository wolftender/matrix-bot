const https = require ('https');
const xml2js = require ('xml2js');
const { LogService } = require ('matrix-bot-sdk');

function sleep (ms) {
    return new Promise (resolve => setTimeout (resolve, ms));
}

function request (url) {
    return new Promise ((resolve, reject) => {
        https.get (url, (res) => {
            res.setEncoding ('utf-8');
            let content = '';

            res.on ('data', (data) => {
                content += data.toString ();
            });
    
            res.on ('end', () => {
                resolve (content);
            });
        }).on ('error', (error) => {
            reject (error);
        });
    });
}

async function getPostData (id) {
    const raw = await request (`https://gelbooru.com/index.php?page=dapi&s=post&q=index&id=${encodeURIComponent(id)}`);

    // parse xml
    const parser = new xml2js.Parser ();
    const data = await parser.parseStringPromise (raw);

    try {
        if (data.posts.post) {
            return data.posts.post[0];
        } else {
            return null;
        }
    } catch (exception) {
        throw new Error (`pasring error: ${exception}`);
    }
}

async function getPostCount (tags) {
    const raw = await request (`https://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=1&tags=${encodeURIComponent(tags)}`);

    // parse xml
    const parser = new xml2js.Parser ();
    const data = await parser.parseStringPromise (raw);

    try {
        const postCount = result.posts.$.count;
        return postCount;
    } catch (exception) {
        throw new Error (`parsing error: ${exception}`);
    }
}

async function getPostImage (tags, id) {
    const raw = await request (`https://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=1&tags=${encodeURIComponent(tags)}&pid=${encodeURIComponent(id)}`);

    // parse xml
    const parser = new xml2js.Parser ();
    const data = await parser.parseStringPromise (raw);

    try {
        if (data.posts.post) {
            return data.posts.post[0];
        } else {
            return null;
        }
    } catch (exception) {
        throw new Error (`pasring error: ${exception}`);
    }
}

function fixFormat (data) {
    const newData = Object.assign ({}, data);

    for (const prop in newData) {
        if (Array.isArray (newData [prop]) && newData[prop].length === 1) {
            newData [prop] = data [prop][0];
        }
    }

    return newData;
}

async function getRandomImage (tags, legacy = false) {
    if (legacy) {
        let count = await getPostCount (tags);
        await sleep (500); // dont overload gel server

        if (count > 20000) {
            count = 20000;
        }

        const rand = Math.floor (Math.random () * count);
        const post = await getPostImage (tags, rand);

        return fixFormat (post);
    } else {
        const post = await getPostImage (`${tags} sort:random`, 0);
        return fixFormat (post);
    }
}

module.exports = {getRandomImage, getPostImage, getPostCount, getPostData};